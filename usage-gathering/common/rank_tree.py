rank_tree = {
    'root_rank':
    {
        'social_rank': {
            # items here are view_name: column_name
            'total_twitter_engagement': 'total_twitter_engagement',
            'avg_daily_twitter_engagement': 'avg_daily_twitter_engagement',
            'week_twitter_engagement': 'week_twitter_engagement',
            'total_reddit_engagement': 'total_reddit_engagement',
            'avg_daily_reddit_engagement': 'avg_daily_reddit_engagement',
            'week_reddit_engagement': 'week_reddit_engagement'
        },
        'usage_rank': {
            # items here are view_name: column_name
            'metcalfe_correlation':'metcalfe_correlation_90',
            'metcalfe_exponent':'metcalfe_exponent_90',
            'normalized_metcalfe':'normalized_metcalfe_90',
            'daily_transactions':'daily_transactions_90',
            'trading_volume':'trading_volume_90'
        },
        'dev_rank': {
            # items here are view_name: column_name
            'github_stargazers':'github_stargazers',
            'github_forks':'github_forks',
            'github_open_issues':'github_open_issues',
            'github_contributors':'github_contributors',
            'github_contributions':'github_contributions'
        },
        'risk_rank': {
            # items here are view_name: column_name
            'alpha':'alpha_90_global',
            'sortino':'sortino_90_global',
            'sharpe':'sharpe_90_global',
            'treynor':'treynor_90_global',
            'upside':'upside_90_global',
            'downside':'downside_90_global',
            'upside/Downside':'up_down_90_global',
            'marketcap':'marketcap_90',
        }
    }
 }