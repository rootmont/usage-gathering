import pandas as pd
import datetime

from db.connect import connection_wrap
from common.config import format_logger
import logging

logger = logging.getLogger('db.blocks')
format_logger(logger)


@connection_wrap
def delete_block_row(blockchain, blocknumber, cursor=None):
    sql = "DELETE FROM blocks WHERE(blockchain, blocknumber) = (%s, %s)"
    blockchain = blockchain.lower()
    cursor.execute(sql, (blockchain, blocknumber))


@connection_wrap
def insert_block(blocknumber, blockchain, datetime, txs, cursor=None):
    datestring = datetime.strftime('%Y/%m/%d')
    dtstring = datetime.strftime('%Y-%m-%d %H:%M:%S')
    sql = (
        "INSERT INTO blocks (`blocknumber`,`blockchain`,`txs`,`my_date`,`tstamp`) " 
        "VALUES (%s, %s, %s, %s, %s) "
        "ON DUPLICATE KEY UPDATE "
        "`txs` = %s, `my_date` = %s, `tstamp` = %s"
    )
    blockchain = blockchain.lower()
    cursor.execute(sql, (blocknumber, blockchain, txs, datestring, dtstring, txs, datestring, dtstring))



@connection_wrap
def get_blocknums_by_date(blockchain, start_date, cursor=None):
    datestring = start_date.strftime('%Y/%m/%d')
    end_date = (datetime.date.today() - datetime.timedelta(days=1)).strftime("%Y/%m/%d")

    sql = (
        "SELECT min(`blocknumber`) as min_block, `my_date` "
        "FROM blocks " 
        "WHERE `blockchain` = %s "
        "AND `my_date` >= %s "
        "AND `my_date` <= %s "
        "GROUP BY `my_date`"
    )
    blockchain = blockchain.lower()
    cursor.execute(sql, (blockchain, datestring, end_date))
    ret = cursor.fetchall()
    if len(ret) > 0:
        last_date = ret[-1]['my_date']
    else:
        last_date = None
    return {x['my_date']: x['min_block'] for x in ret}, last_date


@connection_wrap
def get_blocks(startblock, endblock, blockchain, cursor=None):
    sql = (
        "SELECT txs, blocknumber, tstamp as datetime "
        "FROM blocks " 
        "WHERE `blockchain` = %s "
        "AND `blocknumber` >= %s "
        "AND `blocknumber` <= %s "
    )
    blockchain = blockchain.lower()
    cursor.execute(sql, (blockchain, startblock, endblock))
    ret = cursor.fetchall()
    return list(ret)


@connection_wrap
def get_latest_block(blockchain, cursor=None):
    sql = (
        "SELECT max(blocknumber) as max_block "
        "FROM blocks " 
        "WHERE `blockchain` = %s "
    )
    blockchain = blockchain.lower()
    cursor.execute(sql, blockchain)
    ret = cursor.fetchone()
    return ret['max_block']


@connection_wrap
def get_blocks_from_date(start_date, blockchain, cursor=None):
    datestring = start_date.strftime('%Y/%m/%d')
    sql = (
        "SELECT * "
        "FROM blocks " 
        "WHERE `blockchain` = %s "
        "AND `my_date` >= %s "
    )
    blockchain = blockchain.lower()
    cursor.execute(sql, (blockchain, datestring))
    ret = cursor.fetchall()
    return pd.DataFrame(ret)


@connection_wrap
def get_date_for_block(blocknumber, blockchain, cursor=None):
    sql = (
        "SELECT my_date as date "
        "FROM blocks " 
        "WHERE `blockchain` = %s "
        "AND `blocknumber` = %s "
    )
    blockchain = blockchain.lower()
    blocknumber = int(blocknumber, 16)
    cursor.execute(sql, (blockchain, blocknumber))
    ret = cursor.fetchone()
    if ret is None:
        return None
    return ret['date'].strftime('%Y/%m/%d')


@connection_wrap
def get_dates_for_blockchain(blockchain, min_blocknumber, max_blocknumber=None, cursor=None):
    sql = (
        "SELECT my_date as date, blocknumber "
        "FROM blocks " 
        "WHERE `blockchain` = '%s' "
        "AND `blocknumber` >= %s "
        % (blockchain.lower(), min_blocknumber)
    )
    if max_blocknumber is not None:
        sql += "AND `blocknumber` <= {}".format(max_blocknumber)
    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchall()
    if ret is None:
        return {}
    return {
        x['blocknumber']: x['date'].strftime('%Y/%m/%d')
        for x in ret
    }



@connection_wrap
def get_min_blocknum_for_date(blockchain, date, cursor=None):
    datestring = date.strftime('%Y/%m/%d')
    sql = (
        "SELECT min(`blocknumber`) as blocknum "
        "FROM blocks " 
        "WHERE `blockchain` = %s "
        "AND `my_date` = %s "
    )
    blockchain = blockchain.lower()
    cursor.execute(sql, (blockchain, datestring))
    ret = cursor.fetchone()
    if ret is None:
        return None
    return ret['blocknum']


