import datetime
from db.coin import get_coin_id_for_name
import logging
from common.config import format_logger
import pandas as pd

from db.connect import connection_wrap, timing, get_latest_date

logger = logging.getLogger('social_db')
format_logger(logger)


@connection_wrap
def get_social_ranks_for_date(target_date: datetime.date, cursor=None):
    sql = (
        'SELECT token_name, social_rank_global as social_rank '
        'FROM social '
        'INNER JOIN coin '
        'ON coin.coin_id = social.coin_id '
        'WHERE my_date = DATE("%s") '
        % (target_date)
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    df = pd.DataFrame(ret, index=[x['token_name'] for x in ret], columns=['social_rank'])
    return df.fillna(0)


@connection_wrap
def get_social_stats_for_date(coin_id: int, target_date: datetime.date, cursor=None):
    sql = (
        'SELECT * '
        'FROM social '
        'WHERE my_date = DATE("%s") '
        'AND coin_id = %d '
        % (target_date, coin_id)
    )
    cursor.execute(sql)
    ret = cursor.fetchone()
    if ret is None:
        logger.warning('could not find social for coin id {} date {}'.format(coin_id, target_date))
        return None
    df = pd.Series(ret)
    df.drop(['updated', 'my_date'], inplace=True)
    # TODO: remove this wart, issue is duplicate columns with different names, but we need both for right now
    df.rename(index={'social_rank_global': 'social_rank'})
    return df.fillna(0).to_dict()


def get_latest_social_stats(coin_id: int):
    twitter_date = get_latest_date('total_twitter_engagement', 'social')
    reddit_date = get_latest_date('total_reddit_engagement', 'social')
    if reddit_date is None:
        target_date = twitter_date
    elif twitter_date is None:
        target_date = reddit_date
    else:
        target_date = min(twitter_date, reddit_date)
    return get_social_stats_for_date(coin_id=coin_id, target_date=target_date)


@connection_wrap
def insert(item: tuple, cursor=None):
    name, cols = item
    coin_id = get_coin_id_for_name(name)
    if coin_id is None: return None
    datestring = cols['date'].strftime('%Y-%m-%d')
    del(cols['date'])
    set = ','.join(['{} = {}'.format(k, round(v,14)) for k, v in cols.items()])
    sql = (
        "INSERT INTO social "
        "SET %s, "
        "coin_id = %d, my_date = DATE('%s') "
        "ON DUPLICATE KEY UPDATE "
        "%s"
        % (set, coin_id, datestring, set)
    )
    logger.debug(sql)
    cursor.execute(sql)


def get_latest_social_ranks():
    target_date = get_latest_date('social_rank_global', 'social')
    return get_social_ranks_for_date(target_date)
