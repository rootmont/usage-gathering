from typing import List
import datetime

from db.connect import connection_wrap


def _insert_update_sql() -> str:
    return "INSERT INTO `current_stats` (coin_id, token_name, price, price_open, volume, supply, marketcap) VALUES (%s, %s, %s, %s, %s, %s, %s) ON" +\
    " DUPLICATE KEY UPDATE price=%s, price_open=%s, volume=%s, supply=%s, marketcap=%s"


@connection_wrap
def insert_update_batch(stats: List, cursor=None) -> None:
    for stat in stats:
        sql = _insert_update_sql()
        cursor.execute(sql, (
            stat["coid_id"],
            stat["token_name"],
            stat["price"],
            stat["price_open"],
            stat["volume"],
            stat["supply"],
            stat["marketcap"],
            stat["price"],
            stat["price_open"],
            stat["volume"],
            stat["supply"],
            stat["marketcap"]
        ))


@connection_wrap
def get_all_prices(cursor=None):
    sql = "SELECT price, price_open, marketcap, token_name FROM current_stats"
    cursor.execute(sql)
    return cursor.fetchall()


@connection_wrap
def get_one(token_name: str, cursor=None) -> dict:
    sql = 'SELECT price, volume, supply, marketcap from current_stats where token_name = %s'
    cursor.execute(sql, token_name)
    ret = cursor.fetchone()
    return ret


@connection_wrap
def get_mcaps_today(cursor=None) -> dict:
    datestring = datetime.date.today().strftime('%Y-%m-%d')
    sql = 'SELECT token_name, marketcap FROM current_stats WHERE updated >= DATE("%s")' % datestring
    cursor.execute(sql)
    return cursor.fetchall()