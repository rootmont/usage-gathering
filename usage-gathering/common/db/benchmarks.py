from db.connect import connection_wrap
import logging

logger = logging.getLogger('db')

digits = 15

@connection_wrap
def insert(args, cursor=None):
    date, cluster_type, cluster_name, metric, mean, std, minimum, first_qtr, median, third_qtr, max, count = args
    datestring = date.strftime('%Y/%m/%d')
    sql = (
        "REPLACE INTO `benchmarks` "
        "SET `my_date` = DATE('{}'), "
        "`cluster_type` = '{}', "
        "`cluster_name` = '{}', "
        "`metric` = '{}', "
        "`mean` = {}, "
        "`std` = {}, "
        "`min` = {}, "
        "`25%` = {}, "
        "`50%` = {}, "
        "`75%` = {}, "
        "`max` = {}, "
        "`count` = {}"
        .format(
            datestring,
            cluster_type,
            cluster_name,
            metric,
            round(mean,digits),
            round(std,digits),
            round(minimum,digits),
            round(first_qtr,digits),
            round(median,digits),
            round(third_qtr,digits),
            round(max,digits),
            count)
    )
    logger.debug('Attempting %s' % sql)
    cursor.execute(sql)
    cursor.connection.commit()
    logger.debug('Successful %s' % sql)


@connection_wrap
def get_benchmarks_for_date_type(date, cluster_type, cursor=None):
    datestring = date.strftime('%Y/%m/%d')
    sql = (
        "SELECT * "
        "FROM `benchmarks` "
        "WHERE `my_date` = DATE('%s') "
        "AND `cluster_type` = '%s' "
        % (datestring, cluster_type)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    return ret


@connection_wrap
def get_last_date(cursor=None):
    sql = (
        "SELECT max(my_date) as max_date "
        "FROM benchmarks "
    )
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret['max_date']


def get_latest_benchmarks_for_type(cluster_type):
    max_date = get_last_date()
    return get_benchmarks_for_date_type(date=max_date, cluster_type=cluster_type)

