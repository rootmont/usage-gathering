import logging
from db.connect import connection_wrap, get_latest_date
from db.coin import get_coin_id_for_name
import pandas as pd
import datetime

logger = logging.getLogger('db')
risk_columns = ['alpha','beta', 'cov','downside','upside','sharpe','sortino','treynor','up_down', 'price', 'r_squared']


@connection_wrap
def insert(item, cursor=None):
    name, cols = item
    coin_id = get_coin_id_for_name(name)
    if coin_id is None: return None
    datestring = cols['date'].strftime('%Y/%m/%d')
    del(cols['date'])
    set = ','.join(['%s = %s' % (k ,round(v ,14)) for k ,v in cols.items()])
    sql = (
            "INSERT INTO risk "
            "SET %s, "
            "coin_id = %d, "
            "my_date = DATE ('%s') "
            "ON DUPLICATE KEY UPDATE "
            "%s "
            % (set, coin_id, datestring, set)
    )

    logger.debug('Attempting %s' % sql)
    cursor.execute(sql)
    logger.debug('Successful %s' % sql)


@connection_wrap
def get_columns_for_coin_date(columns, name, date, cursor=None):
    coin_id = get_coin_id_for_name(name)
    if coin_id is None: return None
    select = ','.join(columns)
    datestring = date.strftime('%Y/%m/%d')
    sql = (
            "SELECT %s "
            "FROM risk "
            "WHERE my_date = DATE('%s') "
            "AND coin_id = %d "
            % (select, datestring, coin_id)
    )

    cursor.execute(sql)
    ret = cursor.fetchone()

    return ret


@connection_wrap
def get_risk_ranks_for_date(date=None, cursor=None):
    if date is None:
        date = get_latest_date(table='risk',metric='risk_90_rank_global')
    datestring = date.strftime('%Y/%m/%d')
    sql = (
        "SELECT risk_90_rank_global as risk_rank, token_name "
        "FROM risk "
        "INNER JOIN coin "
        "ON coin.coin_id = risk.coin_id "
        "WHERE my_date = DATE('%s') "
        % datestring
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    df = pd.Series(data=[x['risk_rank'] for x in ret], index=[x['token_name'] for x in ret])
    return df


@connection_wrap
def get_up_down_ranks_for_date(date, cursor=None):
    datestring = date.strftime('%Y/%m/%d')
    sql = (
        "SELECT up_down_90_rank_global as up_down_rank, token_name "
        "FROM risk "
        "INNER JOIN coin "
        "ON coin.coin_id = risk.coin_id "
        "WHERE my_date = %s "
    )
    cursor.execute(sql, (datestring))
    ret = list(cursor.fetchall())
    df = pd.Series(data=[x['risk_rank'] for x in ret], index=[x['token_name'] for x in ret])
    return df


def get_latest_risk_raw(coin_id: int):
    max_date = get_latest_date(coin_id=coin_id, table='risk', metric='risk_90_rank_global')
    return get_risk_raw(coin_id, max_date)


@connection_wrap
def get_risk_raw(coin_id: int, date: datetime.date, cursor=None):
    sql = (
        "SELECT * FROM risk WHERE coin_id = %s AND my_date = %s"
    )
    cursor.execute(sql, [coin_id, date])
    return cursor.fetchone()
