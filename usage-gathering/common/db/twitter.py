import datetime
import logging
import json
from typing import Union, List
import pandas as pd

from db.connect import connection_wrap, timing

logger = logging.getLogger('social_db')


@connection_wrap
def get_all(cursor=None):
    sql = (
        "SELECT COUNT(*) as tweets, created_at as my_date, token_name, likes, replies, retweets "
        "FROM twitter "
        "INNER JOIN coin "
        "ON coin.ticker_symbol = twitter.ticker "
        "GROUP BY token_name, my_date "
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    df = pd.DataFrame(ret, columns=['my_date', 'token_name', 'likes', 'replies', 'retweets', 'tweets'])
    return df

