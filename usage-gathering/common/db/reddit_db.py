import datetime
import logging
import json
from typing import Union, List
import pandas as pd

from db.connect import connection_wrap, timing
from common.config import format_logger

logger = logging.getLogger('reddit_db')
format_logger(logger)

class RedditComment(object):
    def __init__(self, identifier: str, author: str, time: datetime.datetime):
        self._identifier: str = identifier
        self._author: str = author
        self._time: datetime.datetime = time

    def __eq__(self, other):
        if type(other) != RedditComment:
            return False
        if self._author == other.author and self._time.timestamp() == other.time.timestamp() and \
                self._identifier == other.identifier:
            return True
        return False

    def hash(self):
        return hash(self._author.__hash__() + self._time.__hash__())

    @property
    def author(self):
        return self._author

    @property
    def time(self):
        return self._time

    @property
    def identifier(self):
        return self._identifier

    def before(self, day: Union[datetime.datetime, datetime.date]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._time.date() < day

    @classmethod
    def from_json(cls, j: dict):
        return cls(j["id"], j["author"], datetime.datetime.utcfromtimestamp(j["time"]))

    def to_json(self):
        return {
            'id': self._identifier,
            'author': self._author,
            'time': int(self._time.timestamp())
        }


class RedditPost(object):
    def __init__(self, identifier: str, author: str, time: Union[datetime.datetime, str], comments: int):
        self._identifier = identifier
        self._author = author
        if type(time) is not datetime.datetime:
            self._time = datetime.datetime.utcfromtimestamp(int(time))
        else:
            self._time: datetime.datetime = time
        self._comments: int = comments

    def __eq__(self, other):
        if type(other) != RedditPost:
            return False
        if self._identifier == other.identifier:
            return True
        return False

    def __hash__(self):
        return hash(self._identifier.__hash__() + self._author.__hash__() + self.timestamp.__hash__())

    def __str__(self):
        return "<RedditPost, id: {} author: {} timestamp: {} comments: {}>"\
            .format(self._identifier, self._author, self.timestamp, self._comments)

    @property
    def comments(self):
        return self._comments

    @property
    def author(self):
        return self._author

    @property
    def time(self):
        return self._time

    @property
    def timestamp(self):
        return int(self._time.timestamp())

    @property
    def identifier(self):
        return self._identifier

    """
    def append_comments(self, comments) -> None:
        if type(comments) == List:
            self._comments += comments
        else:
            self._comments.append(comments)
    """

    def before(self, day: Union[datetime.datetime, datetime.date]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._time.date() < day

    def after(self, day: Union[datetime.datetime, datetime.date]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._time.date() >= day

    """
    def comments_before(self, day: Union[datetime.datetime, datetime.date]) -> List[RedditComment]:
        return list(filter(lambda x: x.before(day), self._comments))
    """

    @classmethod
    def from_json(cls, j: dict):
        return cls(
            j["id"],
            j["author"],
            datetime.datetime.utcfromtimestamp(j["time"]),
            j["comments"]
        )

    def to_json(self):
        return {
            'id': self._identifier,
            'author': self._author,
            'time': int(self._time.timestamp()),
            'comments': self._comments,
        }


class SubredditPosts(object):
    def __init__(self, subreddit: str, token_name: str, posts: List[RedditPost] = None):
        self._subreddit: str = subreddit
        self._token_name: str = token_name
        self._reddit_posts: List[RedditPost] = posts
        if posts is None:
            self._reddit_posts = []

    @timing
    def append_posts(self, posts: List[RedditPost]) -> bool:
        """
        Appends new posts to
        :param posts: RedditPosts to add to the reddit_posts variable
        :return: bool - whether or not new posts were added
        """
        appended = False
        for p in posts:
            # We want to check that it's not already in the list of posts but not linearly
            if len(self._reddit_posts) == 0 or \
                    p.timestamp > self._reddit_posts[0].timestamp or \
                    p.timestamp < self._reddit_posts[len(self._reddit_posts) - 1].timestamp:
                appended = True
                self._reddit_posts.append(p)
            else:
                # merge results -> if in etc... check to see if numbers are different then append
                appended = appended or self.__update_post(p)
        return appended

    @timing
    def sort_posts(self):
        self._reddit_posts.sort(key=lambda x: x.timestamp, reverse=True)

    def num_posts_before(self, day: Union[datetime.datetime, datetime.date]) -> int:
        if type(day) == datetime.datetime:
            day = day.date()
        return len([x for x in self._reddit_posts if x.before(day)])

    def posts_before(self, day: Union[datetime.datetime, datetime.date]) -> List[RedditPost]:
        if type(day) == datetime.datetime:
            day = day.date()
        return list(filter(lambda x: x.before(day), self._reddit_posts))

    def posts_after(self, day: Union[datetime.datetime, datetime.date]) -> List[RedditPost]:
        if type(day) == datetime.datetime:
            day = day.date()
        return list(filter(lambda x: x.after(day), self._reddit_posts))

    def num_posters_before(self, day: Union[datetime.datetime, datetime.date]) -> int:
        if type(day) == datetime.datetime:
            day = day.date()
        return len(set({ x.author for x in self._reddit_posts if x.before(day)}))

    def num_comments_before(self, day: Union[datetime.datetime, datetime.date]) -> int:
        if type(day) == datetime.datetime:
            day = day.date()
        return sum([sum([x.comments for x in self._reddit_posts if x.before(day)])])

    def __update_post(self, post: RedditPost) -> bool:
        updated = False
        for index in range(len(self._reddit_posts)):
            if self._reddit_posts[index] == post:
                # Interested if there were any comments updated
                if self._reddit_posts[index].comments != post.comments:
                    self._reddit_posts[index] = post
                    updated = True
                    logger.debug("Updating post: {}".format(post))
                    break
        return updated

    def latest_post_timestamp(self) -> Union[None, int]:
        if len(self._reddit_posts) == 0:
            return None
        return self._reddit_posts[0].timestamp

    def earliest_post_timestamp(self) -> Union[None, int]:
        if len(self._reddit_posts) == 0:
            return None
        return self._reddit_posts[len(self._reddit_posts) - 1].timestamp

    @property
    def posts(self):
        return self._reddit_posts

    @property
    def token_name(self):
        return self._token_name

    @property
    def subreddit(self):
        return self._subreddit

    @classmethod
    def from_json(cls, j: dict, token_name: str):
        return cls(j["subreddit"], token_name, [RedditPost.from_json(p) for p in j["posts"]])

    def to_json(self):
        return {
            'subreddit': self._subreddit,
            'posts': [p.to_json() for p in self._reddit_posts]
        }


@connection_wrap
def get_coin_id_for_reddit(reddit, cursor=None):
    sql = "SELECT coin_id from coin where reddit = %s"
    cursor.execute(sql, [reddit])
    coin_id = cursor.fetchone()
    return coin_id if coin_id is None else coin_id['coin_id']


@connection_wrap
def get_reddit_entry_for_coin_id(coin_id, cursor=None) -> Union[None, SubredditPosts]:
    sql = "SELECT posts from reddit where coin_id = %s"
    cursor.execute(sql, [coin_id])
    reddit_raw = cursor.fetchone()
    if reddit_raw is None:
        return None
    return SubredditPosts.from_json(json.loads(reddit_raw["posts"]))


@connection_wrap
def save_reddit_information(coin_id: int, reddit_info: SubredditPosts, cursor = None) -> None:
    sql = "INSERT INTO reddit (coin_id, subreddit, posts) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE posts = %s"
    j = json.dumps(reddit_info.to_json())
    cursor.execute(sql, [coin_id, reddit_info.subreddit, j, j])
    logger.info("Saved reddit info for {}".format(reddit_info.subreddit))


def parse_reddit_data(row: SubredditPosts, date: datetime.date) -> dict:
    earliest_post = row.earliest_post_timestamp()
    if earliest_post is None or datetime.datetime.utcfromtimestamp(earliest_post).date() > date:
        return {
            'total_reddit_engagement': 0,
            'avg_daily_reddit_engagement': 0,
            'week_reddit_engagement': 0,
            'token_name': row.token_name
        }
    earliest = datetime.datetime.utcfromtimestamp(earliest_post).date()
    days_old = (date - earliest).days + 1
    total_engagement = row.num_posts_before(date) + row.num_posters_before(date) + row.num_comments_before(date)
    last_week = date - datetime.timedelta(days=7)
    last_week_engagement = row.num_posts_before(last_week) + row.num_posters_before(last_week) + row.num_comments_before(last_week)
    return {
        'total_reddit_engagement': total_engagement,
        'avg_daily_reddit_engagement': total_engagement / days_old,
        'week_reddit_engagement': (total_engagement - last_week_engagement) / 7,
        'token_name': row.token_name
    }


def get_df(data: List[SubredditPosts], date: datetime.date) -> pd.DataFrame:
    l = [parse_reddit_data(x, date) for x in data]
    df = pd.DataFrame(l, columns=['total_reddit_engagement', 'avg_daily_reddit_engagement', 'week_reddit_engagement'], index=[x['token_name'] for x in l])
    return df


@connection_wrap
def get_all_reddit(cursor=None) -> Union[None, List[dict]]:
    sql = 'SELECT * FROM reddit ' \
          'INNER JOIN coin ' \
          'ON coin.coin_id = reddit.coin_id '
    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


def get_all_reddit_objs() -> Union[None, List[SubredditPosts]]:
    ret = get_all_reddit()
    if ret is None:
        return None
    all_reddit_objs = [SubredditPosts.from_json(json.loads(x["posts"]), x['token_name']) for x in ret]
    return all_reddit_objs
