
####################################################################################
# THIS IS JUST AN EXAMPLE FILE, PLEASE COPY AND MODIFY TO THE NEEDS OF YOUR MODULE #
####################################################################################

from flask import Flask
from flask_api import status
from flask import Response
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
import time
import logging
from common.config import format_logger


logger = logging.getLogger('main')
format_logger(logger)
app = Flask(__name__)


@app.route('/')
def root():
    return 'I am a teapot.', 420


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running or scheduler.get_jobs().__len__() == 0:
        return Response("{}", status=status.HTTP_500_INTERNAL_SERVER_ERROR, mimetype='application/json')
    return Response("{}", status=status.HTTP_200_OK, mimetype='application/json')


now = datetime.datetime.utcnow()
jiggle = datetime.timedelta(seconds=15)
if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    scheduler.add_job(
        func=lambda: print('please give me code'),
        trigger='interval',
        minutes=15,
        max_instances=1,
        next_run_time=now + jiggle
    )
    jobs = scheduler.get_jobs()
    logger.debug('Starting scheduler with {} jobs'.format(len(jobs)))
    scheduler.start()
    time.sleep(1)
    try:
        app.run(host='0.0.0.0', port=5000)
    except Exception as e:
        scheduler.shutdown()
