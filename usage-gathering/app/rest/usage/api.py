# coding: utf-8

import logging
import json
import time
import datetime


from flask_restplus import Resource
from app.rest.api import api
from app.rest.usage.serializers import dummy_serializer
from app.usage.usage import ccxtModule
from app.usage.controller import main_call

log = logging.getLogger(__name__)
ns = api.namespace('usage', description='usage gathering')


@ns.route('/run/markets')
class CCXTRunMarkets(Resource):

    def get(self):
        ccxt_instance = ccxtModule()
        return ccxt_instance.iterate_coins_in_markets()


@ns.route('/test')
class UsageProcessingController(Resource):

    def get(self):
        usage_instance = main_call()
        return usage_instance

