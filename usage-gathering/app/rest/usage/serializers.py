import json
from flask_restplus import fields
from app.router import api


dummy_serializer = api.model('CoinDetails', {
    'exchange': fields.String(required=True, description='Name'),
    'coin': fields.String(required=True, description='Name'),
})
