import time
import sys
from concurrent.futures.thread import ThreadPoolExecutor
import requests
import mysql.connector

threads = []
failing_pages = []
data = []
s_data = 0
flag = True
page = 0
fixing_pages = []
page_round = 0

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="password",
    database="missing_coins"
)


def main_call(mycursor, coin):
    init_time = time.time()
    address = coin[1]
    global flag
    global page_round
    global page
    global failing_pages
    global fixing_pages
    global s_data
    global threads
    count_pages = 0
    page = int(coin[3])
    stm = "SELECT url FROM missing_coins.scan where id = %s"
    mycursor.execute(stm, (coin[2],))
    url = mycursor.fetchone()[0]
    print("Creating threads")
    with ThreadPoolExecutor() as executor:
        executor.submit(output_threads, init_time)
        while flag:
            try:
                page += 1
                pending, running = cleaning()
                s_data += 10000
                t = executor.submit(sub_call, page, url, address)
                threads.append(t)
                while pending > 100:
                    pending, running = cleaning()
                count_pages += 1
                if count_pages == page_round:
                    flag = False
            except Exception as e:
                failing_pages.append(page)
                print(e)
                continue
        while len(threads) != 0:
            cleaning()
        print("\nCleaning")
        try:
            while len(failing_pages) != 0:
                try:
                    failing_pages = list(dict.fromkeys(failing_pages))
                    for pag in failing_pages:
                        if pag not in fixing_pages:
                            t = executor.submit(sub_call_failing, pag, url, address)
                            fixing_pages.append(pag)
                            threads.append(t)
                            while pending > 100:
                                pending, running = cleaning()
                        output_threads_failing(init_time, pag)
                except Exception as e:
                    print(e)
                    continue

            while len(threads) != 0:
                cleaning()
                print("\nRetrying: " + str(len(data)))
            print("\nCleaning 2")
            executor.shutdown(wait=True)

            data_count = 0
            all_data_count = 0
            new_data = []
            stm = "INSERT INTO `missing_coins`.`account_info` " \
                  "(`address`, `blockNumber`, `timeStamp`, `hash`, `nonce`, `blockHash`, " \
                  "`transactionIndex`, `from`, `to`, `value`, `gas`, `gasPrice`, `isError`, `txreceipt_status`, " \
                  "`contractAddress`, `cumulativeGasUsed`, `gasUsed`, `confirmations`) VALUES(%s,%s,%s,%s,%s,%s," \
                  "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            for d in data:
                sys.stdout.write("\r{0}".format("Data number: " + str(all_data_count)) + " from " + str(len(data))
                                 + " files")
                sys.stdout.flush()
                new_data.append(d)
                all_data_count += 1
                if ((data_count % 999) != 0) or (data_count == 0):
                    data_count += 1
                else:
                    try:
                        mycursor.executemany(stm, new_data)
                        data_count = 0
                        new_data = []
                    except Exception as e:
                        print(e)

            try:
                sys.stdout.write("\r{0}".format("Data number: " + str(all_data_count)) + " from " + str(len(data)))
                sys.stdout.flush()
                mycursor.executemany(stm, new_data)
                print("\nSetting page: " + str(page))
                stm = "UPDATE `missing_coins`.`address` SET `page` =  %s WHERE `id` = %s;"
                mycursor.execute(stm, (page, coin[0],))
            except Exception as e:
                print(e)

            print("\nData saved")
            print("Time Elapsed: " + str(int(time.time() - init_time)) + " seconds")
        except Exception as e:
            print(e)


def sub_call(page, url, address):
    try:
        global flag
        global data
        global failing_pages
        querystring = {"module": "account", "action": "txlist", "page": page, "sort": "asc",
                       "apikey": "YourApiKeyToken",
                       "address": address}
        # 0x46b9ad944d1059450da1163511069c718f699d31

        headers = {
            'User-Agent': "PostmanRuntime/7.15.2",
            'Accept': "/",
            'Cache-Control': "no-cache",
            'Postman-Token': "87048051-b87d-4040-b0a3-f17c3bc30ef4,b3579570-9cf4-4728-8372-6b5c0177cf62",
            'Host': "api.etherscan.io",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", url, headers=headers, params=querystring, timeout=4)
        temp = response.json()
        len_temp = len(temp['result'])
        if len_temp == 0:
            flag = False
        else:
            for info in temp['result']:
                tempInfo = (1, info['blockNumber'], info['timeStamp'], info['hash'], info['nonce'], info['blockHash']
                            , info['transactionIndex'], info['from'], info['to'], info['value'], info['gas']
                            , info['gasPrice'], info['isError'], info['txreceipt_status'], info['contractAddress']
                            , info['cumulativeGasUsed'], info['gasUsed'], info['confirmations'])
                data.append(tempInfo)
                # print("temp:" + str(tempInfo))
    except Exception as e:
        # print(e)
        failing_pages.append(page)


def sub_call_failing(page, url, address):
    try:
        global flag
        global data
        global fixing_pages
        global failing_pages
        querystring = {"module": "account", "action": "txlist", "page": page, "sort": "asc",
                       "apikey": "YourApiKeyToken",
                       "address": address}
        # 0x46b9ad944d1059450da1163511069c718f699d31

        headers = {
            'User-Agent': "PostmanRuntime/7.15.2",
            'Accept': "/",
            'Cache-Control': "no-cache",
            'Postman-Token': "87048051-b87d-4040-b0a3-f17c3bc30ef4,b3579570-9cf4-4728-8372-6b5c0177cf62",
            'Host': "api.etherscan.io",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
        response = requests.request("GET", url, headers=headers, params=querystring, timeout=6)
        temp = response.json()
        len_temp = len(temp['result'])
        if len_temp == 0:
            flag = False
        else:
            for info in temp['result']:
                tempInfo = (1, info['blockNumber'], info['timeStamp'], info['hash'], info['nonce'], info['blockHash']
                            , info['transactionIndex'], info['from'], info['to'], info['value'], info['gas']
                            , info['gasPrice'], info['isError'], info['txreceipt_status'], info['contractAddress']
                            , info['cumulativeGasUsed'], info['gasUsed'], info['confirmations'])
                data.append(tempInfo)
            if page in failing_pages:
                failing_pages.remove(page)
            if page in fixing_pages:
                fixing_pages.remove(page)
    except Exception as e:
        print(e)
        fixing_pages.remove(page)


def cleaning():
    count_pending = 0
    count_running = 0
    for t in threads:
        try:
            if t.done():
                threads.remove(t)
            elif t.running():
                count_running += 1
            else:
                count_pending += 1
        except:
            continue
    return count_pending, count_running


def output_threads(init_time):
    global page
    time.sleep(3)
    while len(threads) > 1:
        pending, running = cleaning()
        sys.stdout.write("\r{0}".format("Page: " + str(page)) + ". Failing pages: " + str(len(failing_pages))
                         + ". Threads Pending: " + str(pending) + ". Threads Running: " + str(running) +
                         ". Time Elapsed: " + str(int(time.time() - init_time)) + " seconds" + ". Pages: "
                         + str(page) + ". Data: " + str(len(data)) + "/" + str(s_data))
        sys.stdout.flush()


def output_threads_failing(init_time, page):
    time.sleep(3)
    while len(threads) > 1:
        pending, running = cleaning()
        sys.stdout.write("\r{0}".format("Page: " + str(page)) + ". Failing pages left: " + str(len(failing_pages))
                         + ". Threads Pending: " + str(pending) + ". Threads Running: " + str(running) +
                         ". Time Elapsed: " + str(int(time.time() - init_time)) + " seconds" + ". Pages: "
                         + str(page) + ". Data: " + str(len(data)) + "/" + str(s_data))
        sys.stdout.flush()


def controller():
    global page
    global flag
    global s_data
    global data
    global page_round
    mycursor = mydb.cursor()
    stm = "SELECT * FROM missing_coins.config"
    mycursor.execute(stm)

    response = mycursor.fetchone()
    tic = response[1]
    page_round = response[2]
    stm = "SELECT * FROM missing_coins.address where tic = %s"
    mycursor.execute(stm, (tic,))
    coins = mycursor.fetchall()
    if tic == 1:
        tic = 0
    else:
        tic = 1
    for coin in coins:
        data = []
        page = int(coin[3])
        flag = True
        s_data = 0
        print("Coin: " + str(coin))
        main_call(mycursor, coin)
        print("Setting tic: " + str(tic))
        stm = "UPDATE `missing_coins`.`address` SET `tic` =  %s WHERE `id` = %s;"
        mycursor.execute(stm, (tic, coin[0]))

    stm = "UPDATE `missing_coins`.`config` SET `tic` =  %s WHERE `id` = 1;"
    mycursor.execute(stm, (tic,))

    mydb.commit()
    mydb.close()


controller()
