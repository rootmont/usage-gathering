import requests


def test_ark():

    url = "https://api.ark.io/api/transactions"

    querystring = {"orderBy": "timestamp:desc?page=1"}

    headers = {
        'API-Version': "2",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Host': "api.ark.io",
        'Accept-Encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.text)
    

def test_nebl():
    url = "http://explorer.nebl.io/ext/getlasttxs/2/1"

    headers = {
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Accept-Encoding': "gzip, deflate",
        'Referer': "http://explorer.nebl.io/ext/getlasttxs/2/1",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)

    print(response.text)


def test_MaidSafeCoin():
    url = "https://api.omniexplorer.info/v1/transaction/address"

    payload = "addr=1ARjWDkZ7kT9fwjPrjcQyvbXDkEySzKHwu&page=0"
    headers = {
        'Content-Type': "application/x-www-form-urlencoded",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Host': "api.omniexplorer.info",
        'Accept-Encoding': "gzip, deflate",
        'Content-Length': "46",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)


def test_Tether():
    url = "https://api.omniexplorer.info/v1/transaction/address"

    payload = "addr=3MbYQMMmSkC3AgWkj9FMo5LsPTW1zBTwXL&page=0"
    headers = {
        'Content-Type': "application/x-www-form-urlencoded",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Host': "api.omniexplorer.info",
        'Accept-Encoding': "gzip, deflate",
        'Content-Length': "46",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)


def test_EOS():
    url = "https://block.io/api/v2/get_my_addresses/"

    querystring = {"api_key": "2185-bef3-f262-a5c6"}

    headers = {
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Host': "block.io",
        'Accept-Encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.text)


def test_TRON():
    url = "https://apilist.tronscan.org/api/transaction"

    querystring = {"sort": "-timestamp", "count": "true", "limit": "20", "start": "0",
                   "start_timestamp": "1548000000000", "end_timestamp": "1548056638507"}

    headers = {
        'Accept': "*/*",
        'Cache-Control': "no-cache",
        'Host': "apilist.tronscan.org",
        'Accept-Encoding': "gzip, deflate",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.text)


def test_Burst():
    url = "https://explore.burst.cryptoguru.org/"

    querystring = {"p": "1"}

    headers = {
        'sec-fetch-mode': "cors",
        'accept-encoding': "gzip, deflate, br",
        'accept-language': "en,es;q=0.9,la;q=0.8,en-US;q=0.7",
        'accept': "text/html, */*; q=0.01",
        'referer': "https://explore.burst.cryptoguru.org/",
        'authority': "explore.burst.cryptoguru.org",
        'x-requested-with': "XMLHttpRequest",
        'sec-fetch-site': "same-origin",
        'Cache-Control': "no-cache",
        'Host': "explore.burst.cryptoguru.org",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.text)


def test_Skycoin():
    url = "https://explorer.skycoin.net/api/transactions"

    querystring = {"confirmed": "1", "addrs": "7cpQ7t3PZZXvjTst8G7Uvs7XH4LeM8fBPD,6dkVxyKFbFKg9Vdg6HPg1UANLByYRqkrdY",
                   "verbose": "1"}

    headers = {
        'sec-fetch-mode': "cors",
        'accept-encoding': "gzip, deflate, br",
        'accept-language': "en,es;q=0.9,la;q=0.8,en-US;q=0.7",
        'accept': "text/html, */*; q=0.01",
        'referer': "https://explore.burst.cryptoguru.org/",
        'authority': "explore.burst.cryptoguru.org",
        'x-requested-with': "XMLHttpRequest",
        'sec-fetch-site': "same-origin",
        'Cache-Control': "no-cache",
        'Host': "explorer.skycoin.net",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.text)


print("Ark")
test_ark()
print("Nebl")
test_nebl()
print("MaidSafeCoin")
test_MaidSafeCoin()
print("Tether")
test_Tether()
print("Eos")
test_EOS()
print("TRON")
test_TRON()
print("Burst")
test_Burst()
print("Skycoin")
test_Skycoin()
