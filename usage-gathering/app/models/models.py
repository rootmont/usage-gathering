from app.flask_factory import db

from sqlalchemy import Index

from sqlalchemy.schema import FetchedValue

# coding: utf-8
from sqlalchemy import Column, DateTime, ForeignKey, Integer, Numeric, SmallInteger, String, Text
from sqlalchemy.dialects.mysql import INTEGER, TINYINT, LONGTEXT, SMALLINT, TIMESTAMP, BIGINT

from sqlalchemy.schema import FetchedValue
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from app.models.mixins import TimeStampsMixin, SoftDeletesMixin

from datetime import datetime, timedelta

Base = declarative_base()
metadata = Base.metadata


class CCXTHourlyInformation(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'coins_hourly_information'


    id = db.Column(db.Integer, primary_key=True)
    exchange = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    coin = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    open = db.Column(db.Float('20, 10'), nullable=True)
    high = db.Column(db.Float('20, 5'), nullable=True)
    low = db.Column(db.Float('20, 5'), nullable=True)
    close = db.Column(db.Float('20, 5'), nullable=True)
    volume = db.Column(db.Float('20, 5'), nullable=True)
    timestamp = db.Column(db.DateTime)


class CCXTMarkets(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'markets'

    id = db.Column(db.Integer, primary_key=True)
    exchange = db.Column(db.String(191, 'utf8mb4_unicode_ci'), index=True)
    symbol = db.Column(db.String(191, 'utf8mb4_unicode_ci'), index=True)
    inprogress =  db.Column(db.Boolean,  default=False)
    completed = db.Column(db.Boolean,  default=False)
    error = db.Column(db.Boolean,  default=False)
    incremental_inprogress = db.Column(db.Boolean, default=False)
    incremental_completed = db.Column(db.Boolean, default=False)
    incremental_error = db.Column(db.Boolean, default=False)
    full_inprogress = db.Column(db.Boolean, default=False)
    full_completed = db.Column(db.Boolean, default=False)
    full_error = db.Column(db.Boolean, default=False)


class DailyBlock(db.Model):
    __tablename__ = 'daily_block'

    blocknumber = db.Column(db.Integer, primary_key=True)
    blockchain = db.Column(String(191, 'utf8mb4_unicode_ci'), index=True)
    txs = db.Column(db.Integer)
    my_date =  db.Column(db.DateTime)
    tstamp = db.Column(db.DateTime)
    ntx = db.Column(db.Integer)
    coin = db.Column(String(191, 'utf8mb4_unicode_ci'), index=True)
    blocknumber_ = db.Column(db.Integer)
    datetime = db.Column(db.DateTime)