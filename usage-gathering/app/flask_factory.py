from flask import Flask
from flask_dotenv import DotEnv
from flask_migrate import Migrate
from sqlalchemy import create_engine
from flask_sqlalchemy import SQLAlchemy
import pymysql
pymysql.install_as_MySQLdb()

from app.usage.config import connection_string

import configparser

import click

app = Flask(__name__)

env = DotEnv()
env.init_app(app)

app.config['SQLALCHEMY_DATABASE_URI'] = connection_string
app.config['SECRET_KEY'] = 'supersecret'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_POOL_SIZE'] = 10
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 30
app.config['SQLALCHEMY_POOL_RECYCLE'] = 60
app.config['SQLALCHEMY_MAX_OVERFLOW'] = 5

settings  = app.config

db = SQLAlchemy(app)

migrate = Migrate(app, db)


@app.cli.command()
def run_markets():

    print("Initializing command for ccxt")

    from app.usage.usage import ccxtModule

    usage_instance = ccxtModule()

    usage_instance.iterate_coins_in_markets()

    return True

@app.cli.command()
def run_controller():

    print("Initializing command for usage")
    return True


def url_obtain(_url):

    import json
    from app.usage.controller import main_call

    try:
        
        address = _url.split("/")

        address = address[len(address)-1]

        dic = json.dumps({"result":"true"})

        main_call(address)

        print("Starting process")

        return dic

    except Exception as error:

        print(error)

        return False

@app.cli.command()
def command_test():

    print("Running threads")
    
    result = url_obtain("https://etherscan.io/token/0x46b9ad944d1059450da1163511069c718f699d31")

    print("Finishing threads")

    return result
