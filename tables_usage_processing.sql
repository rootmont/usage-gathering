-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema blockchain_metadata
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema blockchain_metadata
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `blockchain_metadata` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `blockchain_metadata` ;

-- -----------------------------------------------------
-- Table `blockchain_metadata`.`account_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blockchain_metadata`.`account_info` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `address` INT(11) NOT NULL,
  `blockNumber` LONGTEXT NULL DEFAULT NULL,
  `timeStamp` LONGTEXT NULL DEFAULT NULL,
  `hash` LONGTEXT NULL DEFAULT NULL,
  `nonce` LONGTEXT NULL DEFAULT NULL,
  `blockHash` LONGTEXT NULL DEFAULT NULL,
  `transactionIndex` LONGTEXT NULL DEFAULT NULL,
  `from` LONGTEXT NULL DEFAULT NULL,
  `to` LONGTEXT NULL DEFAULT NULL,
  `value` LONGTEXT NULL DEFAULT NULL,
  `gas` LONGTEXT NULL DEFAULT NULL,
  `gasPrice` LONGTEXT NULL DEFAULT NULL,
  `isError` LONGTEXT NULL DEFAULT NULL,
  `txreceipt_status` LONGTEXT NULL DEFAULT NULL,
  `contractAddress` LONGTEXT NULL DEFAULT NULL,
  `cumulativeGasUsed` LONGTEXT NULL DEFAULT NULL,
  `gasUsed` LONGTEXT NULL DEFAULT NULL,
  `confirmations` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `address`),
  INDEX `fk_address_idx` (`address` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `blockchain_metadata`.`scan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blockchain_metadata`.`scan` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `url` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `blockchain_metadata`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blockchain_metadata`.`address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `scan` INT(11) NULL DEFAULT NULL,
  `page` VARCHAR(45) NULL DEFAULT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_scan_idx` (`scan` ASC) VISIBLE,
  CONSTRAINT `fk_scan`
    FOREIGN KEY (`scan`)
    REFERENCES `blockchain_metadata`.`scan` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
